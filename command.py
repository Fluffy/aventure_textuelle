class Word:
    def __init__(self, class_name, names):
        self.class_name = class_name
        self.names = names

    def __repr__(self):
        return self.class_name

    def means(self, o):
        if o == self.class_name:
            return True
        for name in self.names:
            if name == o:
                return True
        return False


class Command:
    def __init__(self, type, args, from_location, update_last_answer=True):
        # Type = a Word
        self.type = type
        # args = Dict of arguments
        # each arguments is a Word
        self.args = args
        # Location which will execute the command if found
        self.location = from_location

        self.update_last_answer = update_last_answer

    def __repr__(self):
        name = "None"
        if self.location is not None:
            name = self.location.name
        txt = f"Command({self.type}) of location {name}"
        if len(self.args) == 0:
            txt += " (no arguments)."
        else:
            txt += " with argument "
            for arg in self.args.keys():
                txt += f' "{arg}" = "{self.args[arg]}",'
            txt = txt[:-1]
            txt += "."
        return txt
