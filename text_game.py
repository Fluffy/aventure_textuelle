import os
import random
import pickle
import logging

import params
from player import Player
from command import Command, Word


class TextGame:
    def __init__(self):
        self.bot = None
        self.player = None
        random.seed()

        self.text_help = ""
        with open(params.help_file, "r") as f:
            self.text_help = f.read()

    def answer(self, text, status=None):
        logging.info(f'answering to "{text}"')
        answer = ""
        if self.bot is not None:
            if status is None:
                logging.error(f"The bot doesn't know to which status answer !")
            else:
                self.player = self.load_player(status)
                if self.player is not None and len(self.player.locations) > 0:
                    answer = self.construct_answer(text)
                    self.bot.publish(status, answer)
                    self.save_player()
                elif self.player is None:
                    logging.error(f"The player is not well defined !")
                elif len(self.player.locations) == 0:
                    logging.error(f"Player's location is not well defined.")
        else:
            logging.error("The bot is not defined !")
        logging.info(f"my answer is : {answer}")

    def load_player(self, status):
        filename = f"{params.player_save_folder}{status['account']['acct']}.dat"
        if os.path.exists(filename):
            with open(filename, "rb") as input:
                data = pickle.load(input)
                location_hierarchy = data["location_hierarchy"]
                variables = data["variables"]
                items = data["items"]
                return Player(self, status, location_hierarchy, variables, items)
        else:
            return Player(self, status, [params.first_location], {}, {})  # tochange

    def save_player(self):
        filename = f"{params.player_save_folder}{self.player.status['account']['acct']}.dat"
        with open(filename, "wb") as output:  # Overwrites any existing file.
            pickle.dump(
                {
                    "location_hierarchy": self.player.location_hierarchy,
                    "variables": self.player.variables,
                    "items": self.player.items,
                },
                output,
                pickle.HIGHEST_PROTOCOL,
            )
        pass

    def construct_answer(self, text):
        commands = self.player.commands
        command_not_understood = Command(
            type=Word("not_understood", []),
            args=[],
            from_location=self.player.get_local_location(),
            update_last_answer=False,
        )

        words = text.split(" ")
        num_command = 0

        answer = ""
        command = None
        while len(answer) == 0 and num_command < len(commands):
            # logging.info(f"Trying {commands[num_command]}")
            command = self.find_command(commands[num_command], words)
            # logging.info(f"{command}")
            answer = self.run_command(command)
            # if command is not None:
            #     logging.info(f"{num_command}/{len(commands)} : {command} ===> {answer}")
            num_command += 1
        # If we found no command : not understood
        if num_command == len(commands) and len(answer) == 0:
            command = command_not_understood
            answer = self.run_command(command)

        if command.update_last_answer:
            self.player.variables["last_answer"] = answer

        logging.info(f"Command found : {command}")

        return self.customize(answer)

    def find_command(self, command, words):
        if command.type.class_name == params.everything:
            return command
        c = None
        args = None
        for word in words:
            if command.type.means(word):
                c = command
                break
        if c is not None:
            arguments = c.args
            for arg_name in arguments.keys():
                arg_list = arguments[arg_name]
                index = 0
                for arg in arg_list:
                    arg_found = False
                    if arg.class_name == params.everything:
                        text = ""
                        for w in words:
                            text += w + " "
                        if len(text) > 0:
                            text = text[:-1]
                        c.args[arg_name] = text
                        arg_found = True
                    else:
                        for w in words:
                            if arg.means(w):
                                c.args[arg_name] = arg.class_name
                                arg_found = True
                                break
                        if arg_found:
                            break
                if not arg_found:
                    c.args[arg_name] = None
        return c

    def run_command(self, command):
        if command is None:
            return ""
        elif command.type.means("not_understood"):
            return self.not_understood()
        else:
            return command.location.run_command(self.player, command)

    def not_understood(self):
        lines = []
        with open(params.not_understood_file, "r") as f:
            lines = f.readlines()
        text_nu = lines[random.randrange(len(lines))].strip()
        return text_nu + "\n---------\nJe n'ai pas compris ton dernier message...\n" + self.text_help

    def customize(self, text):
        if self.player is None:
            return text
        else:
            custom_text = text
            words = custom_text.split("\\")
            for word in words:
                if word[:4] == "var[":
                    index = word.find("]")
                    var_name = word[4:index]
                    custom_text = custom_text.replace("\\" + word[: index + 1], str(self.player.get_variable(var_name)))
            return custom_text
