import os
import time
import logging
from html.parser import HTMLParser

import mastodon
from unidecode import unidecode

import params


class Parser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.txt = ""

    def handle_data(self, data):
        data = (
            str(data).lower().replace("'", " ").replace(".", " ").replace(",", " ").replace("?", " ").replace("!", " ")
        )
        self.txt += unidecode(data)

    def handle_endtag(self, tag):
        if tag == "p":
            self.txt += "\n\n"

    def handle_starttag(self, tag, attrs):
        if tag == "br":
            self.txt += "\n"

    def get_text(self):
        txt = self.txt
        self.txt = ""
        return txt.strip()


class BotListener(mastodon.StreamListener):
    def __init__(self, game):
        self.game = game
        self.parser = Parser()

    def on_notification(self, notification):
        if notification["type"] == "mention":
            status = notification["status"]
            if status["visibility"] == "direct":
                content = str(status["content"])
                logging.info(f"toot is {content}")
                self.parser.feed(content)
                content = self.parser.get_text()

                self.game.answer(content, status)


class Bot:
    def __init__(self, game):
        self.game = game
        self.game.bot = self
        self.listener = BotListener(game)

        api_url = ""
        max_toot_chars = -1
        with open(params.api_url_file, "r") as f:
            api_url = f.readline().strip()
            max_toot_chars = int(f.readline().strip())
        self.api_mastodon = mastodon.Mastodon(
            client_id=params.appCred, access_token=params.accountCred, api_base_url=api_url
        )
        logging.info(
            f"Bot connected to it's account on {api_url}. Maximum number of characters per toot is {max_toot_chars}."
        )
        self.max_toot_chars = max_toot_chars

    def publish(self, answer_to, text):
        at_user = "@" + answer_to["account"]["acct"]
        if len(at_user) >= int(self.max_toot_chars / 2):
            logging.error(f"User name is too long. ({at_user})")
        else:
            text = text.split(params.toot_separator)
            text = [at_user + " " + t for t in text]
            for t in text:
                # while(len(t) > self.max_toot_chars):
                #     cut_text.append(t[:self.max_toot_chars])
                #     t = at_user + " " + t[self.max_toot_chars+1:]
                # cut_text.append(t)
                medias = []
                CW = None

                words = t.split("\\")  # we separate  by words
                for i in range(len(words)):
                    if len(medias) < 4 and words[i][:6] == "media[":
                        index = words[i].find("]")
                        if index > 6:
                            media_name = params.media_folder + words[i][6:index]  # extraction of name in \media[name]
                            if os.path.exists(media_name):
                                medias.append(self.api_mastodon.media_post(media_name)["id"])
                            words[i] = words[i][index + 1 :]  # we delete the media command

                    elif words[i][:3] == "CW[":
                        index = words[i].find("]")
                        if index > 3:
                            CW = words[i][3:index]
                            words[i] = words[i][index + 1 :]

                if (
                    len(medias) > 0 or CW is not None
                ):  # we modified the text by removing a media command so we reconstruct the text without them
                    t = ""
                    for word in words:
                        t += word

                while len(t) > self.max_toot_chars:
                    answer_to = self.api_mastodon.status_post(
                        t[: self.max_toot_chars - 1],
                        in_reply_to_id=answer_to["id"],
                        visibility="direct",
                        spoiler_text=CW,
                    )
                    t = at_user + " " + t[self.max_toot_chars - 1 :]

                answer_to = self.api_mastodon.status_post(
                    t[: self.max_toot_chars - 1],
                    in_reply_to_id=answer_to["id"],
                    visibility="direct",
                    media_ids=tuple(medias),
                    spoiler_text=CW,
                )
                time.sleep(1)

    def listen(self):
        logging.info("Now listening to notifications...")
        connection_handle = self.api_mastodon.stream_user(self.listener, run_async=True, reconnect_async=True)
        while connection_handle.is_alive():
            time.sleep(1)

    def emergency_contact(self):
        contact = ""
        with open(params.emergencyContactFile) as f:
            contact = f.readline().strip()
        if len(contact) > 1:
            self.api_mastodon.status_post(f"@{contact} {params.emergencyMessage}", visibility="direct")
