from runpy import run_path

from command import Word, Command
import params


class Location:
    def __init__(self, name):
        # toi = Word("toi", ["ursula"])
        # moi = Word("moi", ["naomi", "nausicaa"])
        # chapeau = Word("chapeau", ["casquette", "couvre-chef", "bob"])
        # look = Command(
        #     type=Word("regarder", ["voir", "observer", "inspecter"]), args={"entité": [toi, moi]}, from_location=self
        # )
        # put = Command(
        #     type=Word("mettre", ["enfiler", "poser"]), args={"entité": [chapeau], "sur": [moi, toi]}, from_location=self
        # )
        # self.commands = [look, put]
        self.name = name
        self.script = run_path(params.location_folder + self.name + ".py")
        self.commands = self.script["get_commands"]()
        for command in self.commands:
            command.location = self

    def at_enter(self, player):
        return self.script["at_enter"](player)

    def run_command(self, player, command):
        return self.script["run_command"](player, command)
        # if command.type.means("regarder"):
        #     return "MDRRRRR"
        # elif command.type.means("mettre"):
        #     return "POPOPOPOOOO"
        # return ""
