import logging

from location import Location
import params


class Item:
    def __init__(self, internal_name):
        self.name_singular = ""
        self.name_plural = ""
        self.description = ""
        filename = f"{params.item_folder}{internal_name}.dat"
        with open(filename, "r") as f:
            self.name_singular = f.readline().strip()
            self.name_plural = f.readline().strip()
            self.description = f.readline().strip()


class Player:
    def __init__(self, game, status, location_hierarchy, variables, items):
        self.game = game
        self.status = status
        self.variables = variables  # variables["game_internal_name"] = any_value
        self.items = items  # items["game_internal_name"] = {"object":Item_object, "quantity":qty}
        self.locations = []
        self.commands = []

        self.location_hierarchy = location_hierarchy
        self.generate_commands()

    def teleport_local(self, location_name):
        self.location_hierarchy[-1] = location_name
        self.generate_commands()
        return self.locations[-1].at_enter(self)

    def teleport_global(self, location_hierarchy):
        self.location_hierarchy = location_hierarchy
        self.generate_commands()
        return self.locations[-1].at_enter(self)

    def generate_commands(self):
        self.locations = []
        self.commands = []
        for location_name in self.location_hierarchy:
            self.locations.append(Location(location_name))

        for location in self.locations:
            self.commands.extend(location.commands)

    def get_variable(self, name, default=0):
        if name in self.variables:
            return self.variables[name]
        else:
            self.variables[name] = default
            return default

    def get_local_location(self):
        return self.locations[-1]

    def add_item(self, item_name, qty=1):
        if item_name in self.items:
            self.items[item_name]["quantity"] += qty
        else:
            self.items[item_name] = {}
            self.items[item_name]["object"] = Item(item_name)
            self.items[item_name]["quantity"] = qty

    def remove_item(self, item_name, qty=1):
        if item_name in self.items:
            self.items[item_name]["quantity"] -= qty
            if self.items[item_name]["quantity"] <= 0:
                del self.items[item_name]
        else:
            logging.error(f"{item_name} not found in player's items")
