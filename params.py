# Debug and log parameters
debug = True

logDir = "data/log"
log = "data/log/log.txt"

reconnect_time = 30

# Login parameters
first_login_file = "data/app_login_password.dat"
appCred = "data/appCred.dat"
accountCred = "data/accountCred.dat"
api_url_file = "data/api_url.dat"

# Emergency contact parameters
emergencyContactFile = "data/emergencyContact.dat"
emergencyMessage = "Bonjour !\nJ'ai rencontré une erreur tout récemment alors je voulais juste te prévenir pour que tu puisses regarder d'où ça vient.\nBon courage !"

bdd_folder = "data/bdd/"
player_save_folder = bdd_folder + "save/"
media_folder = bdd_folder + "media/"
location_folder = bdd_folder + "location/"
font_folder = bdd_folder + "font/"
item_folder = bdd_folder + "item/"


first_location = "not_in_game"
not_understood_file = "data/not_understood.dat"
help_file = "data/help.dat"
everything = "*everything*"

toot_separator = "\\break"
