import logging
import os, sys
from os.path import dirname, join, abspath

sys.path.insert(0, abspath(join(dirname(__file__), "../../..")))

from command import Word, Command
import params

balai = Word("balai", ["balais"])
manche = Word("manche", [])
tete = Word("tete", [])
coffre = Word("coffre", ["malle"])
toot = Word("toot", ["pouet"])
tuyau = Word("tuyau", ["tuyaux"])
tootomatic = Word("tootomatic", [])
porte = Word("porte", [])
dehors = Word("dehors", ["exterieur"])
atelier = Word("atelier", ["autour"])
armoire = Word("armoire", [])
harmoniseur = Word("harmoniseur", [])
frigo = Word("froidimension", ["frigo", "frigidaire", "refrigerateur"])
cupcake = Word("cupcake", ["cake", "gateau", "cupcakes", "cakes", "gateaux"])
bouteille = Word("bouteille", ["eau"])
toilettes = Word("toilettes", ["toilette", "chiotte", "chiottes", "WC"])
lunettes = Word("lunettes", [])
pipicaca = Word("pipi", ["caca", "crotte"])
sieste = Word("sieste", ["dodo"])

regarder = Command(
    type=Word("regarder", ["examiner", "voir", "inspecter"]), args={}, from_location=None, update_last_answer=True,
)
regarder.args["entité"] = [
    balai,
    manche,
    tete,
    coffre,
    toot,
    tuyau,
    tootomatic,
    porte,
    dehors,
    atelier,
    armoire,
    harmoniseur,
    frigo,
    cupcake,
    bouteille,
    toilettes,
    lunettes,
]

attendre = Command(type=Word("attendre", ["patienter"]), args={}, from_location=None, update_last_answer=True,)

ecouter = Command(type=Word("ecouter", ["entendre"]), args={}, from_location=None, update_last_answer=True,)
ecouter.args["entité"] = [tootomatic, harmoniseur, dehors, frigo]

ecouter2 = Command(
    type=Word("tendre", []), args={"oreille": [Word("oreille", [])]}, from_location=None, update_last_answer=True,
)
ecouter2.args["entité"] = [tootomatic, harmoniseur, dehors, frigo]

aller = Command(type=Word("aller", ["diriger", "marcher"]), args={}, from_location=None, update_last_answer=True,)
aller.args["destination"] = [dehors, toilettes, frigo]

prendre = Command(
    type=Word("prendre", ["tenir", "attraper", "soulever", "ramasser"]),
    args={},
    from_location=None,
    update_last_answer=True,
)
prendre.args["entité"] = [balai, manche, tete, toot, cupcake, bouteille]

utiliser = Command(
    type=Word("utiliser", ["actionner", "tirer", "enfourcher"]), args={}, from_location=None, update_last_answer=True,
)
utiliser.args["entité"] = [balai, tootomatic, harmoniseur, frigo, toilettes]

partir = Command(type=Word("partir", ["sortir", "livrer"]), args={}, from_location=None, update_last_answer=True,)
partir.args["destination"] = [dehors]

porter = Command(
    type=Word("porter", ["habiller", "enfiler", "mettre"]), args={}, from_location=None, update_last_answer=True,
)
porter.args["entité"] = [lunettes, frigo]

enlever = Command(type=Word("enlever", ["retirer"]), args={}, from_location=None, update_last_answer=True,)
enlever.args["entité"] = [lunettes]

manger = Command(
    type=Word("manger", ["croquer", "devorer", "deguster", "engloutir", "sustenter"]),
    args={},
    from_location=None,
    update_last_answer=True,
)
manger.args["entité"] = [cupcake]

boire = Command(type=Word("boire", ["desalterer", "hydrater"]), args={}, from_location=None, update_last_answer=True,)
boire.args["entité"] = [bouteille]

faire = Command(type=Word("faire", []), args={}, from_location=None, update_last_answer=True,)
faire.args["activité"] = [pipicaca, sieste]

uriner = Command(type=Word("uriner", ["pisser"]), args={}, from_location=None, update_last_answer=True,)

defequer = Command(type=Word("defequer", ["chier"]), args={}, from_location=None, update_last_answer=True,)

danser = Command(type=Word("danser", []), args={}, from_location=None, update_last_answer=True,)

dormir = Command(type=Word("dormir", ["reposer"]), args={}, from_location=None, update_last_answer=True,)

assembler = Command(type=Word("assembler", []), args={}, from_location=None, update_last_answer=True,)
assembler.args["entité"] = [balai]

monter = Command(type=Word("monter", []), args={}, from_location=None, update_last_answer=True,)
monter.args["entité"] = [balai]

ouvrir = Command(type=Word("ouvrir", []), args={}, from_location=None, update_last_answer=True,)
ouvrir.args["entité"] = [coffre, porte, armoire, frigo]

frapper = Command(type=Word("frapper", ["taper"]), args={}, from_location=None, update_last_answer=True,)

sentir = Command(type=Word("sentir", ["humer"]), args={}, from_location=None, update_last_answer=True,)
sentir.args["entité"] = [tootomatic, harmoniseur, toilettes, dehors, cupcake]

fermer = Command(type=Word("fermer", ["clore"]), args={}, from_location=None, update_last_answer=False,)


def get_commands():
    return [
        regarder,
        attendre,
        ecouter,
        ecouter2,
        aller,
        prendre,
        utiliser,
        partir,
        porter,
        enlever,
        manger,
        boire,
        faire,
        uriner,
        defequer,
        danser,
        dormir,
        assembler,
        monter,
        ouvrir,
        frapper,
        sentir,
        fermer,
    ]


text_enter = """Ça, c'est moi. \media[witch_selfie.png]

Je suis une sorcière de l'instance Mastodon autogérée witches.camp.

Enfin... Je viens d'arriver, il faut que je prenne mes marques encore, évidemment.

Comme c'est autogéré, on fait tout nous-même ! On s'organise. C'est assez excitant, même si parfois c'est un peu déroutant.
Aujourd'hui, pour la première fois je m'occupe de la livraison de toots.
\\break
C'est pour ça que je suis à l'atelier. Mais... Ça fait 25 minutes que j'y suis et il n'y a toujours personne...

Normalement une sorcière aguerrie devait me guider mais elle n'est pas là.

...


...

Du coup... Je ne sais pas trop quoi faire. Enfin, si en théorie c'est assez simple : je prends mon balai, je prends tous les toots à livrer et je suis les panneaux !

Mais bon, une petite initiation n'aurait pas été de refus ! ^^\"
\\break
...

...

J'en peux plus d'atteeeeeendre. Forcément pour ma première fois en plus.

Faut que je m'occupe je ne peux pas faire qu'attendre. Je pourrais... regarder les toots à livrer ? Ils sont dans le coffre à toots de l'atelier.
"""


def at_enter(player):
    player.variables["atelier.attendre"] = 0
    player.variables["atelier.armoire_ouverte"] = False
    player.variables["atelier.coffre_ouvert"] = False
    player.variables["atelier.manche_pris"] = False
    player.variables["atelier.tête_prise"] = False
    player.variables["atelier.harmoniseur_utilisé"] = False
    player.variables["atelier.lunettes_portées"] = False
    player.variables["atelier.toot_pris"] = False
    player.variables["atelier.cupcake_pris"] = False
    player.variables["atelier.bouteille_prise"] = False
    player.add_item("lunettes")
    return text_enter


atelier_desc = """L'atelier. Une pièce assez grande, à la base elle était pour la réparation des balais mais finalement on a mis le tootomatic et le coffre à toots à livrer ici aussi. En plus il y a une armoire où on stocke les balais. L'harmoniseur est là pour assembler les balais. On a aussi prévu de quoi combler un petit creux avec la froidimension, et à l'arrière se trouvent les toilettes de l'atelier. En face il y a la porte par laquelle je suis entrée."""


def run_command(player, command):
    ct = command.type
    ca = command.args

    if ct.means("regarder"):
        if ca["entité"] is None:
            return atelier_desc

        elif ca["entité"] == ("manche"):
            if not player.variables["atelier.armoire_ouverte"] or player.variables["atelier.harmoniseur_utilisé"]:
                return ""
            else:
                return "Le manche du balai. Long, fin, rectiligne, en vieux chêne, il assure la stabilité dans le couple manche-tête. Il me permet aussi de me diriger quand je suis sur le balai, ce qui n'est pas mal."

        elif ca["entité"] == ("tete"):
            if not player.variables["atelier.armoire_ouverte"] or player.variables["atelier.harmoniseur_utilisé"]:
                return ""
            else:
                return "La tête du balai. Un faisceau de branchettes hautement magiques : un concentré d'énergie instable qui permet de propulser les balais pour peu qu'on arrive à le contrôler."

        elif ca["entité"] == ("balai"):
            if not player.variables["atelier.armoire_ouverte"]:
                return "Bien sur, il faudrait que je me trouve un balai..."
            else:
                if not player.variables["atelier.harmoniseur_utilisé"]:
                    return "J'ai bien pu trouver de quoi faire un balai mais il faudrait que je l'assemble sinon je ne vais rien pouvoir faire avec."
                else:
                    return "J'ai à ma disposition un joli balai fin prêt à être utilisé ! C'est pas n'importe quel balai, c'est un Cleanus 2000 !"

        elif ca["entité"] == ("coffre"):
            if not player.variables["atelier.coffre_ouvert"]:
                return "Le coffre à toots. Un authentique coffre de pirate il parait. Enfin à une époque. Il est relié par des tuyaux au tootomatic qui achemine les toots dans le coffre. Et tous les matins, on va livrer les toots."
            else:
                if not player.variables["atelier.toot_pris"]:
                    return "Le... Le coffre ne contient qu'un seul toot ! Ça c'est la meilleure... L'instance n'a pas été très active il faut croire."
                else:
                    return "Le coffre à toots est vide à présent."

        elif ca["entité"] == ("toot"):
            if not player.variables["atelier.coffre_ouvert"]:
                return ""
            else:
                if not player.variables["atelier.toot_pris"]:
                    return f"""Voilà donc le seul et unique toot à délivrer aujourd'hui. Je devrais le prendre, après tout il va bien falloir le livrer... L'adresse du destinataire au dos est Gargron@mastodon.social.  \\media[toot_to_deliver/{player.variables["username"]}.png]"""
                else:
                    return "*un bruit retentissant se fait entendre*\n\n Le toot est dans mon inventaire. Je peux l'examiner en passant par celui-ci : \"inventaire toot\" \n\n (le bruit semble être celui du 4e mur qui vient d'être cassé. Oupsi.)"

        elif ca["entité"] == ("tuyau"):
            return "Un complexe réseau de tuyaux relie le coffre au tootomatic, et le tootomatic à chaque compte des sorcières de witches.camp. Comme ça on a plus qu'à livrer les nouveaux toots !"

        elif ca["entité"] == ("tootomatic"):
            return "Le tootomatic c'est la grosse machine qui permet aux toots à envoyer de se retrouver à l'atelier, dans le coffre à toots. Elle est entièrement faite en matériaux de récup' mais elle fonctionne très bien, avec un peu d'entretien."

        elif ca["entité"] == ("porte"):
            return "Et bien en voilà une bien belle porte."

        elif ca["entité"] == ("dehors"):
            return "J'admirerai l'extérieur une fois sortie de l'atelier."

        elif ca["entité"] == ("atelier"):
            return atelier_desc

        elif ca["entité"] == ("armoire"):
            return "L'armoire sert à stocker les balais de livraison quand on ne s'en sert pas, ou les balais à réparer s'il y en a."

        elif ca["entité"] == ("harmoniseur"):
            return "L'harmoniseur, c'est la machine qui sert à assembler les manches et les têtes de balai. \nCes deux parties sont d'une nature magique très différente et ne s'assemblent pas naturellement. L'harmoniseur permet de caler les ondes magiques des deux objets pour qu'ils coopèrent et forment le balai, qui est plus que la somme de ses parties. Bref, de la magie de base.\n\n Il est écrit \"PORT DE LUNETTES OBLIGATOIRE\""

        elif ca["entité"] == ("froidimension"):
            dans_froidimension = "La froidimension, c'est un portail vers une dimension qui est toujours à la bonne température pour conserver des aliments. C'est pratique, écolo, et ça ne prend pas de place."
            if not player.variables["atelier.cupcake_pris"] or not player.variables["atelier.bouteille_prise"]:
                dans_froidimension += "\nDedans il y a :\n"
            if not player.variables["atelier.cupcake_pris"]:
                dans_froidimension += "- un cupcake\n"
            if not player.variables["atelier.bouteille_prise"]:
                dans_froidimension += "- une bouteille d'eau"
            return dans_froidimension

        elif ca["entité"] == ("cupcake"):
            if not player.variables["atelier.cupcake_pris"]:
                return "Il reste un joli cupcake dans la froidimension. Au chocolat, je crois."
            else:
                if "cupcake" in player.items:
                    return "Quel cupcake ? Je l'ai mangé déjà..."
                else:
                    return "*un bruit retentissant se fait entendre*\n\n Le cupcake est dans mon inventaire. Je peux l'examiner en passant par celui-ci : \"inventaire cupcake\" \n\n (le bruit semble être celui du 4e mur qui vient d'être cassé. Oupsi.)"

        elif ca["entité"] == ("bouteille"):
            if not player.variables["atelier.bouteille_prise"]:
                return "Il reste une bouteille d'eau fraiche dans la froidimension."
            else:
                if "bouteille" in player.items:
                    return "J'ai fait disparaitre la bouteille après l'avoir bue..."
                else:
                    return "*un bruit retentissant se fait entendre*\n\n La bouteille est dans mon inventaire. Je peux l'examiner en passant par celui-ci : \"inventaire bouteille\" \n\n (le bruit semble être celui du 4e mur qui vient d'être cassé. Oupsi.)"

        elif ca["entité"] == ("toilettes"):
            return "Et bien oui, même les sorcières vont aux toilettes."

        elif ca["entité"] == ("lunettes"):
            return "*un bruit retentissant se fait entendre*\n\n Les lunettes sont dans mon inventaire. Je peux l'examiner en passant par celui-ci : \"inventaire lunettes\" \n\n (le bruit semble être celui du 4e mur qui vient d'être cassé. Oupsi.)"
        else:
            return atelier_desc

    elif ct.means("attendre"):
        if player.variables["atelier.attendre"] < 5:
            player.variables["atelier.attendre"] += 1
            return "tou pi dou pi dou wa... \n \n ... \n \n .......... \n \nToujours personne. Je veux faire quelque chose !"
        else:
            return "MAIS J'EN PEUX PLUS D'ATTENDRE AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHHH"

    elif ct.means("ecouter") or ct.means("tendre"):
        if ca["entité"] is None:
            return "Tout est calme ici... Pas un bruit dehors, et ici, juste les petits bruits habituels du tootomatic et de la froidimension."

        if ca["entité"] == ("tootomatic"):
            return "Le tootomatic ronronne doucement. Malgré la complexité de cette machine, elle est plutôt silencieuse finalement. On entend parfois la vapeur sortir quand il y en a trop."

        elif ca["entité"] == ("harmoniseur"):
            return "Au moins quand on n'utilise pas l'harmoniseur, il ne fait pas de bruit... \n\nPar contre, une fois qu'on lui donne un balai à monter l'harmoniseur produit un vacarme assez caractéristique."

        elif ca["entité"] == ("dehors"):
            return "Je n'entend personne arriver."

        elif ca["entité"] == ("froidimension"):
            return "La froidimension émet une onde sonore pure."

    elif ct.means("aller"):
        if ca["destination"] is None:
            return "Hein ? Aller où ?"

        elif ca["destination"] == ("dehors"):
            if player.variables["atelier.harmoniseur_utilisé"]:
                if player.variables["atelier.toot_pris"]:
                    return player.teleport_local("outside")
                else:
                    return "J'ai mon balai... Je pourrais délivrer le toot. Il faut que je le prenne !"
            else:
                return "Je ne vais quand même pas sortir sans balai !"

        elif ca["destination"] == ("toilettes"):
            return "C'est vrai, il vaut mieux passer aux toilettes avant les livraisons. On n'est jamais trop prudente."

        elif ca["destination"] == ("froidimension"):
            return "L'ouverture de la froidimension est trop petite pour que je puisse rentrer dedans... Et heureusement, je crois."

    elif ct.means("prendre"):
        if ca["entité"] is None:
            return ""

        elif ca["entité"] == ("manche"):
            if not player.variables["atelier.armoire_ouverte"]:
                return ""
            else:
                if not player.variables["atelier.manche_pris"]:
                    player.add_item("manche_balai")
                    player.variables["atelier.manche_pris"] = True
                    return "J'ai le manche du balai avec moi."
                else:
                    return "J'ai déjà pris le manche du balai."

        elif ca["entité"] == ("tete"):
            if not player.variables["atelier.armoire_ouverte"]:
                return ""
            else:
                if not player.variables["atelier.tête_prise"]:
                    player.add_item("tete_balai")
                    player.variables["atelier.tête_prise"] = True
                    return "J'ai la tête du balai avec moi."
                else:
                    return "J'ai déjà pris la tête du balai."

        elif ca["entité"] == "balai":
            if not player.variables["atelier.armoire_ouverte"]:
                return "Les balais doivent se trouver dans l'armoire."
            else:
                if not player.variables["atelier.harmoniseur_utilisé"]:
                    return "Je dois monter moi-même un balai à partir d'une tête et d'un manche."
                else:
                    return "J'ai déjà le balai avec moi."

        elif ca["entité"] == ("toot"):
            if not player.variables["atelier.coffre_ouvert"]:
                return "Les toots doivent être à l'intérieur du coffre à toots"
            else:
                if not player.variables["atelier.toot_pris"]:
                    player.add_item("toot")
                    player.variables["atelier.toot_pris"] = True
                    return f"""Voilà donc le seul et unique toot à délivrer aujourd'hui. Je le prends, après tout il va bien falloir le livrer... L'adresse du destinataire au dos est Gargron@mastodon.social. \\media[toot_to_deliver/{player.variables["username"]}.png]"""
                else:
                    return "J'ai déjà pris le toot."

        elif ca["entité"] == ("cupcake"):
            if not player.variables["atelier.cupcake_pris"]:
                player.add_item("cupcake")
                player.variables["atelier.cupcake_pris"] = True
                return "Je prends ce joli cupcake avec moi... Après tout, je sais que j'en ferai bon usage."
            else:
                return "J'ai déjà pris le cupcake, il n'y en a plus malheureusement..."

        elif ca["entité"] == ("bouteille"):
            if not player.variables["atelier.bouteille_prise"]:
                player.add_item("bouteille")
                player.variables["atelier.bouteille_prise"] = True
                return "Je prends la bouteille avec moi... C'est important d'être hydratée."
            else:
                return "J'ai déjà pris la bouteille, il n'y en a plus."

    elif ct.means("utiliser"):
        if ca["entité"] is None:
            return ""

        elif ca["entité"] == ("balai"):
            if player.variables["atelier.harmoniseur_utilisé"]:
                return "Mmmh... Avant d'utiliser mon balai je devrais surement sortir, ça éviterait des accidents."
            elif player.variables["atelier.armoire_ouverte"]:
                return "Avant de pouvoir utiliser mon balai je devrais l'assembler."
            else:
                return "Mmmh... Il faudrait que je me trouve un balai."

        elif ca["entité"] == ("tootomatic"):
            return "Je ne peux pas vraiment utiliser le tootomatic... Il fonctionne tout seul en récupérant les toots de witches.camp pour les rassembler dans le coffre à toots."

        elif ca["entité"] == ("harmoniseur"):
            if player.variables["atelier.harmoniseur_utilisé"]:
                return "J'ai déjà utilisé l'harmoniseur, j'ai mon balai désormais."
            else:
                if not player.variables["atelier.lunettes_portées"]:
                    return "Safety first. Je devrais mettre mes lunettes."
                else:
                    if player.variables["atelier.tête_prise"]:
                        if player.variables["atelier.manche_pris"]:
                            player.variables["atelier.harmoniseur_utilisé"] = True
                            player.add_item("balai")
                            player.remove_item("manche_balai")
                            player.remove_item("tete_balai")
                            return "Je place la tête et le manche du balai dans les endroits appropriés sur l'harmoniseur et je rabaisse sa calandèle. Un vacarme assourdissant tandis que le manche se rapproche de la tête si énérgétique du balai. Le sol tremble. Une lumière intense est émise. \nPuis, plus rien. Le balai est formé et stable maintenant. Il est encore chaud. L'harmoniseur plonge le balai dans son bac d'eau. Cela produit plein de fumée.\n\n*bip, bip, biiiiip* \\break Ça y est, mon balai est près ! Je le prends avec moi."
                        else:
                            return "Avant de pouvoir utiliser l'harmoniseur, il me faut aussi le manche du balai."
                    else:
                        if player.variables["atelier.manche_pris"]:
                            return "Avant de pouvoir utiliser l'harmoniseur, il me faut aussi la tête du balai."
                        else:
                            return "Avant de pouvoir utiliser l'harmoniseur, il me faut  le manche et la tête d'un balai afin de les assembler."

        elif ca["entité"] == ("froidimension"):
            return "Je n'ai rien à mettre dans la froidimension, je préfère avoir tout sur moi au cas où."

        elif ca["entité"] == ("toilettes"):
            return "C'est vrai, il vaut mieux passer aux toilettes avant les livraisons. On n'est jamais trop prudente."

    elif ct.means("partir"):
        if player.variables["atelier.harmoniseur_utilisé"]:
            if player.variables["atelier.toot_pris"]:
                return player.teleport_local("outside")
            else:
                return "J'ai mon balai... Je pourrais délivrer le toot. Il faut que je le prenne !"
        else:
            return "Je ne vais quand même pas sortir sans balai !"

    elif ct.means("porter"):
        if ca["entité"] is None:
            return ""

        elif ca["entité"] == "froidimension":
            return "Je n'ai rien à mettre dans la froidimension, je préfère avoir tout sur moi au cas où."
        elif ca["entité"] == ("lunettes"):
            if not player.variables["atelier.lunettes_portées"]:
                player.variables["atelier.lunettes_portées"] = True
                return "Lunettes : mises !"
            else:
                return "C'est bon, j'ai déjà mis mes lunettes."

    elif ct.means("enlever"):
        if ca["entité"] is None:
            return ""

        elif ca["entité"] == ("lunettes"):
            if player.variables["atelier.lunettes_portées"]:
                player.variables["atelier.lunettes_portées"] = False
                return "Lunettes : enlevées !"
            else:
                return "C'est bon, j'ai déjà enlevé mes lunettes."

    elif ct.means("manger"):
        if ca["entité"] is None:
            return "Je ne vais tout de même pas manger ça."

        elif ca["entité"] == ("cupcake"):
            if "cupcake" in player.items:
                player.remove_item("cupcake")
                return "Quel doux bonheur...  *miam*"
            else:
                return "Je n'ai pas de cupcake à manger... malheureusement."

    elif ct.means("boire"):
        if ca["entité"] is None:
            return "Je ne vais tout de même pas boire ça."

        elif ca["entité"] == ("bouteille"):
            if "bouteille" in player.items:
                player.remove_item("bouteille")
                return 'Ça désaltère !\n\n"Detritus Disparitio !"\n\nLa bouteille utilisée disparait instantanément.'
            else:
                return "Je n'ai rien à boire... malheureusement."

    elif ct.means("faire"):
        if ca["activité"] is None:
            return ""

        elif ca["activité"] == ("pipi"):
            return "C'est vrai, il vaut mieux passer aux toilettes avant les livraisons. On n'est jamais trop prudente."

        elif ca["activité"] == ("sieste"):
            return "Pas le temps, on se reposera plus tard."

        else:
            return "OK BOOMER"

    elif ct.means("uriner"):
        return "C'est vrai, il vaut mieux passer aux toilettes avant les livraisons. On n'est jamais trop prudente."

    elif ct.means("defequer"):
        return "C'est vrai, il vaut mieux passer aux toilettes avant les livraisons. On n'est jamais trop prudente."

    elif ct.means("danser"):
        return "*entame un tango endiablé avec soi-même*"

    elif ct.means("dormir"):
        return "Pas le temps, on se reposera plus tard."

    elif ct.means("assembler"):
        if ca["entité"] is None:
            return ""

        elif ca["entité"] == ("balai"):
            if player.variables["atelier.harmoniseur_utilisé"]:
                return "J'ai déjà assemblé mon balai."
            else:
                return "Il me faut utiliser l'harmoniseur pour ça."

    elif ct.means("monter"):
        if ca["entité"] is None:
            return ""

        elif ca["entité"] == ("balai"):
            if player.variables["atelier.harmoniseur_utilisé"]:
                return "Mmmh... Avant de monter sur mon balai je devrais surement sortir, ça éviterait des accidents."
            elif player.variables["atelier.armoire_ouverte"]:
                return "Avant de pouvoir monter sur mon balai je devrais l'assembler."
            else:
                return "Mmmh... Il faudrait que je me trouve un balai."

    elif ct.means("ouvrir"):
        if ca["entité"] is None:
            return ""

        elif ca["entité"] == ("coffre"):
            if player.variables["atelier.coffre_ouvert"]:
                return "J'ai déjà ouvert le coffre à toots."
            else:
                player.variables["atelier.coffre_ouvert"] = True
                return "J'ouvre le coffre à toots pour voir son contenu et...\n\nBizarre,il n'y en a qu'un. Je devrais le prendre."

        elif ca["entité"] == ("porte"):
            return "Elle est déjà ouverte. Il fait beau."

        elif ca["entité"] == ("armoire"):
            if player.variables["atelier.armoire_ouverte"]:
                return "J'ai déjà ouvert l'armoire."
            else:
                player.variables["atelier.armoire_ouverte"] = True
                return "J'ouvre l'armoire. Dedans il y a la tête et le manche d'un balai à assembler."

        elif ca["entité"] == ("froidimension"):
            return "Il n'y a pas vraiment besoin d'ouvrir et de fermer la froidimension... En fait, ça n'a pas vraiment de sens."

    elif ct.means("frapper"):
        return "Mais qu'est-ce qui me prend ?!? Je ne vais quand même pas céder à la violence ?!?"

    elif ct.means("sentir"):
        if ca["entité"] is None:
            return "Aucune odeur particulière vient à mon nez..."

        elif ca["entité"] == ("tootomatic"):
            return "Le tootomatic sent l'acier et le caoutchouc chauffé. Mais l'odeur n'est pas trop forte."

        elif ca["entité"] == ("harmoniseur"):
            return "L'harmoniseur fait plus de bruits que d'odeurs."

        elif ca["entité"] == ("toilettes"):
            return "Les toilettes sont propres, il y a une odeur d'huile essentielle de verveine."

        elif ca["entité"] == ("dehors"):
            return "Ca sent le printemps. Une délicieuse odeur d'herbe fraîche et de fleurs sur le point d'éclore. Malheureusement, la pièce sent aussi encore un peu le renfermé."

        elif ca["entité"] == ("cupcake"):
            if player.variables["atelier.cupcake_pris"] and "cupcake" not in player.items:
                return "Je ne peux plus sentir le délicieux cupcake maintenant que je l'ai mangé... Quelle tristesse..."
            return "Ce cupcake a l'air moelleux à point. Miam !"

        else:
            return "Ca sent le printemps. Une délicieuse odeur d'herbe fraîche et de fleurs sur le point d'éclore. Malheureusement, la pièce sent aussi encore un peu le renfermé."

    elif ct.means("fermer"):
        return "Pas la peine."

    else:
        return ""

    return ""
