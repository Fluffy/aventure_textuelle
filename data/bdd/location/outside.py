import os, sys
from os.path import dirname, join, abspath

sys.path.insert(0, abspath(join(dirname(__file__), "../../..")))

from command import Word, Command
import params


def get_commands():
    return [
        Command(type=Word("commencer", []), args={}, from_location=None, update_last_answer=True),
        Command(type=Word(params.everything, []), args={}, from_location=None, update_last_answer=False),
    ]


text_enter = """
Quel bonheur de sentir l'air frais du matin, la journée qui commence ! Toujours personne ? Tant pis. Je vais livrer le toot moi-même. Comme une grande. Plus qu'à m'envoler vers... l'aventure !
\\break
Bon, bah j'espère que ça t'a plu, c'était la démo du jeu, maintenant pour avoir la suite faut payer.

Non je rigole, y'a juste pas de suite, désooooo

(mais tu peux quand même me payer (en poupitude))

(y'a plus rien à voir, circulez)

(je vous aime)
"""


def at_enter(player):
    return text_enter


instructions = """\\CW[instructions pour jouer] Bonjour ! Je suis un bot d'aventure textuelle sur mastodon qui réagit aux DMs.

Je vais te décrire une scène et tu devras me répondre simplement ce que tu voudras faire.

Par exemple, je pourrai te décrire une cuisine et tu pourras me demander \"regarder le four\". Alors je te donnerai la description du four, et ainsi de suite.

Pour (re)commencer une nouvelle partie avec moi, tu peux me dire en DM \"commencer\".

De manière générale, essayer de me demander des choses simples et de séparer les mots clairement pour que je les comprenne.
"""


def run_command(player, command):
    if command.type.means("commencer"):
        return player.teleport_global(["introduction"])
    elif command.type.means(params.everything):
        return instructions
    else:
        return ""
