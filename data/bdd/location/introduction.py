import os, sys
from os.path import dirname, join, abspath

sys.path.insert(0, abspath(join(dirname(__file__), "../../..")))

from PIL import Image, ImageFont, ImageDraw
from urllib.request import urlretrieve

from command import Word, Command
import params


def create_toot(player):
    toot_filename = f"""{params.media_folder}toot_to_deliver/{player.variables["username"]}.png"""
    base_filename = f"""{params.media_folder}toot_to_deliver/base/{player.variables["toot_to_deliver"]}.png"""
    filter_filename = f"""{params.media_folder}toot_to_deliver/base/round_filter.png"""
    avatar_filename, _ = urlretrieve(player.status["account"]["avatar_static"])

    avatar = Image.open(avatar_filename)
    avatar = avatar.resize((52, 52), Image.NEAREST)

    base = Image.open(base_filename)

    mask_avatar = None
    if len(avatar.split()) == 4:
        # avatar has transparency
        r, g, b, a = avatar.split()
        avatar = Image.merge("RGB", (r, g, b))
        mask_avatar = Image.merge("L", (a,))

    base.paste(avatar, (12, 11), mask=mask_avatar)

    # avatar is a square image, whereas in mastodon, usually avatar are in a rounded square. We apply a "filter" to round the boundaries using transparency.
    round_filter = Image.open(filter_filename)
    r, g, b, a = round_filter.split()
    round_filter = Image.merge("RGB", (r, g, b))
    mask = Image.merge("L", (a,))

    base.paste(round_filter, (12, 11), mask=mask)

    font1 = ImageFont.truetype(params.font_folder + "Roboto-Medium.ttf", 16)
    font2 = ImageFont.truetype(params.font_folder + "Roboto-Regular.ttf", 15)

    draw_base = ImageDraw.Draw(base)

    name = player.status["account"]["username"]
    while 75 + draw_base.textsize(name, font=font1)[0] > 385:
        name = name[:-4] + "..."
    draw_base.text((75, 8), name, font=font1, fill="white")
    second_text_pos_x = 75 + draw_base.textsize(name, font=font1)[0] + 4
    while second_text_pos_x + draw_base.textsize(f"@{name}", font=font2)[0] > 385 and len(name) > 4:
        name = name[:-4] + "..."
    if len(name) > 4:
        draw_base.text((second_text_pos_x, 9), f"@{name}", font=font2, fill="#5e6781")
    base.save(toot_filename)


def get_commands():
    return [
        Command(type=Word("furry", ["1"]), args={}, from_location=None, update_last_answer=True),
        Command(type=Word("ananas", ["2", "🍍"]), args={}, from_location=None, update_last_answer=True),
        Command(
            type=Word("admin", ["3", "sys", "blague", "blagues"]), args={}, from_location=None, update_last_answer=True
        ),
        Command(type=Word("awoo", ["4"]), args={}, from_location=None, update_last_answer=True),
        Command(type=Word("patriarcat", ["5", "marre"]), args={}, from_location=None, update_last_answer=True),
        Command(type=Word("linux", ["6", "marche"]), args={}, from_location=None, update_last_answer=True),
        Command(type=Word("crepe", ["7", "ferai"]), args={}, from_location=None, update_last_answer=True),
        Command(type=Word("chocolatine", ["couque"]), args={}, from_location=None, update_last_answer=True),
        Command(
            type=Word("8", ["dit"]),
            args={"pain": [Word("pain", [])], "chocolat": [Word("chocolat", [])]},
            from_location=None,
            update_last_answer=True,
        ),
        Command(
            type=Word("pain", []),
            args={"chocolat": [Word("chocolat", [])]},
            from_location=None,
            update_last_answer=True,
        ),
        Command(type=Word(params.everything, []), args={}, from_location=None, update_last_answer=False),
    ]


intro_phrase = """Hey, ça me fait plaisir de te voir !

Tu viens de commencer une nouvelle partie. Enfin, non. Pas encore.

D'abord j'ai besoin d'en savoir un peu plus pour toi. Je voudrais que tu répondes le plus sincèrement possible à la question suivante.
\\break
\\CW[nourriture]
Quelle phrase te définit le plus ?


1 - Je ne suis pas furry.

2 - Avant tout je suis un ananas 🍍 .

3 - Je suis admin sys et je fais des blagues.

4 - Awoo.

5 - J'en ai un petit peu marre, parfois, du patriarcat.

6 - "Écoute, j'utilise linux et chez moi ça marche..."

7 - Je me ferai bien une petite crêpe.

8 - On dit une chocolatine.

"""


def at_enter(player):
    # reset the player
    username = player.status["account"]["acct"]
    player.variables = {"username": username}
    player.items = {}
    return intro_phrase


chocolatine = "Hum... Je pense que tu as voulu dire pain au chocolat... Recommence. \\break" + intro_phrase


def run_command(player, command):
    ct = command.type
    ca = command.args
    ok_commands = ["furry", "ananas", "admin", "awoo", "patriarcat", "linux", "crepe"]
    for cname in ok_commands:
        if ct.means(cname):
            player.variables["toot_to_deliver"] = cname
            create_toot(player)
            return player.teleport_global(["in_game_functions", "atelier"])

    if ct.means("chocolatine"):
        return chocolatine
    elif ct.means("8"):
        if ca["pain"] is not None and ca["chocolat"] is not None:
            player.variables["toot_to_deliver"] = "pain_au_chocolat"
            create_toot(player)
            return player.teleport_global(["in_game_functions", "atelier"])
        else:
            return chocolatine
    elif ct.means("pain"):
        if ca["chocolat"] is not None:
            player.variables["toot_to_deliver"] = "pain_au_chocolat"
            create_toot(player)
            return player.teleport_global(["in_game_functions", "atelier"])
        else:
            return chocolatine

    elif ct.means(params.everything):
        return intro_phrase
    return intro_phrase
