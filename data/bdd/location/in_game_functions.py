import logging
import os, sys
from os.path import dirname, join, abspath

sys.path.insert(0, abspath(join(dirname(__file__), "../../..")))

from command import Word, Command
import params


def get_commands():
    return [
        Command(type=Word("aide", []), args={}, from_location=None, update_last_answer=False),
        Command(
            type=Word("recommencer", []),
            args={"confirmation": [Word("confirmer", [])]},
            from_location=None,
            update_last_answer=False,
        ),
        Command(type=Word("rememorer", []), args={}, from_location=None, update_last_answer=False),
        Command(
            type=Word("inventaire", []),
            args={"objet": [Word(params.everything, []),]},
            from_location=None,
            update_last_answer=False,
        ),
    ]


help_phrase = ""
with open(params.help_file, "r") as f:
    help_phrase = f.read()


def at_enter(player):
    return ""


def run_command(player, command):
    ct = command.type
    ca = command.args

    if ct.means("aide"):
        return help_phrase
    elif ct.means("recommencer"):
        if ca["confirmation"] is not None:
            return player.teleport_global(["introduction"])
        else:
            return 'Pour recommencer une partie, dis-moi "recommencer confirmer".'
    elif ct.means("rememorer"):
        return player.get_variable("last_answer", "")
    elif ct.means("inventaire"):
        toot = ca["objet"].split(" ")
        if toot[-1] == "":
            toot = toot[:-1]
        index = toot.index("inventaire")
        if index == len(toot) - 1:
            text = "Dans ma besace se trouve :\n"
            for key in player.items:
                item = player.items[key]
                qty = item["quantity"]
                item_name = item["object"].name_singular
                if qty > 1:
                    item_name = item["object"].name_plural
                text += f"- {qty} {item_name}\n"
            return text
        else:
            item = toot[index + 1]
            for internal_name in player.items:
                logging.info(f"test {internal_name}")
                if (
                    item == player.items[internal_name]["object"].name_singular
                    or item == player.items[internal_name]["object"].name_plural
                ):
                    return f"""{player.items[internal_name]["object"].name_singular} :

Description : {player.items[internal_name]["object"].description}

Quantité : {player.items[internal_name]["quantity"]}"""
            return "Je ne trouve pas d'objet correspondant dans mon inventaire..."

    return ""
